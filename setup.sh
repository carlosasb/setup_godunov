#!/bin/bash

# test to see if a valid directory was given in $1
if [[ ! -d $2 ]]
then
    echo "##### Argument "$2" is NOT a valid directory! #####"
    exit 1
fi

ldir=$(pwd)

read -a gradius <<< `$ldir/rot_curve/rot_curve.x`

for (( cunt=0; $cunt < ${#gradius[@]}; cunt++ ))
do
    echo ${gradius[cunt]}
done

echo "======================================================"

pdir=$2
cool=$1
mkdir -p "$pdir/$cool"

#if  [[ -e $3 ]]
#then
#    setup=$3
#else
#    echo "WARNING! No datafile provided!" 
#    echo "Using default datafile setup!"
#    echo "There is a 15 seconds delay to continue."
#    sleep 15
setup="default.txt"
#fi

declare -a varname init_val final_val

while read line
do
    read -a fout <<< `./lineparser.x "$line" " "`
    varname[k]=${fout[0]}
    init_val[k]=${fout[1]}
    final_val[k]=${fout[2]}
    k=$((k+1))
done < $setup

vali=${init_val[2]}
i=$((vali/5))
i=$i
jobnamei="spiral_arms"
jobname[0]=$jobnamei
tmax=${init_val[0]}
while [[ $tmax -le ${final_val[0]} ]]
do
    pitarm=${init_val[1]}
    while [[ $pitarm -le ${final_val[1]} ]]
    do
        vel=${init_val[2]}
        vali=${init_val[2]}
        i=$((vali/5))
        i=$((i+1))
        while [ $vel -le ${final_val[2]} ]
        do
            smass=${init_val[3]}
            while [[ $smass -le ${final_val[3]} ]]
            do
                sig=${init_val[4]}
                while [[ $sig -le ${final_val[4]} ]]
                do
                    jobname[1]="${varname[2]}${vel}_${varname[4]}${sig}_${varname[0]}${tmax}"
                    sed -i "s/${jobname[0]}/${jobname[1]}/g" 'job.script'
                    tdir="$pdir/$cool/gamma$tmax/ang$pitarm/vel$vel/smass$smass/sig$sig/"
                    echo ""
                    echo $tdir
                    echo "gamma $tmax pitarm $pitarm gradius ${gradius[i]} smass $smass sig $sig"
                    mkdir -p $tdir
                    cp "job.script" $tdir
                    cd $pdir
                    echo $(pwd)
                    $ldir/config_file/config_file.x "gamma" $tmax "pitarm" $pitarm "gradius" ${gradius[i]} "smass" $smass "sig" $sig
                    cp "carlos.in" $tdir
                    rm "carlos.in"
                    cd $ldir
                    jobname[0]=${jobname[1]}
                    sig=$((sig+100))
                done
                smass=$((smass+10))
            done
            vel=$((vel+10))
            i=$((i+1))
        done
        pitarm=$((pitarm+5))
    done
    tmax=$((tmax+1))
done
sed -i "s/${jobname[0]}/$jobnamei/g" 'job.script'
