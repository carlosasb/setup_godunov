/*=========================================================//
// LINEPARSER: parses lines using a user defined delimiter //
//                                                         //
//      usage: ./lineparser.x LINE DELIM                   //
//                                                         //
// parameters:  LINE => line to be parsed                  //
//             DELIM => delimiter used for parsing         //
//=========================================================*/

#include <iostream>
#include <string>
#include <sstream>
#include <vector>

using namespace std;

int main(int argc, char* argv[])
{
/*  p_str => string containing the tokens
 *  ss    => stream created to receive LINE
 *  elems => vector containing the tokens extracted from p_str
 *  delim => delimiter used to parse the DELIM*/
    string p_str;
    stringstream ss;
    vector<string> elems;
    char delim = *argv[argc-1];
    ss << argv[1];
/*  reads the stream until delim and saves it to the elems vector*/    
    while (getline(ss, p_str, delim))
    {
        if (!p_str.empty())
	{
            elems.push_back(p_str);
	}
    }
/*  sends the elements in elems to the std::cout stream*/    
    for (int i; i<elems.size(); i++)
    {
        cout << elems[i] << "\n";
    }
    return 0;
}
