include make.config
#----------------------------------------------------------------------#
# C++ compiler

ifeq ($(CPPCOMPILER), GNU)
CPPC     = g++
endif # COMPILER

#----------------------------------------------------------------------#
# Flags

ifeq ($(HOST), gina)
TDIR     = $(HOME)/alphacrucis/$(SIMS)
TESTDIR  = $(HOME)/testes
endif # HOST

ifeq ($(HOST), cygwin)
TDIR     = /cygdrive/o/alphacrucis/$(SIMS)
TESTDIR  = $(HOME)/teste
endif # HOST

ifeq ($(HOST), hermes)
TDIR     = /cygdrive/n/alphacrucis/$(SIMS)
TESTDIR  = $(HOME)/teste
endif # HOST

ifeq ($(HOST), kino_alphacrucis)
TDIR     = $(HOME)/code_doutorado/new_version/alphacrucis/$(SIMS)
TESTDIR  = $(HOME)/teste
endif # HOST

ifeq ($(HOST), kino_gina)
TDIR     = $(HOME)/gina/alphacrucis/$(SIMS)
TESTDIR  = $(HOME)/teste
endif # HOST

ifeq ($(COOL), Y)
COOLDIR  = cooling
else
COOLDIR  = no_cooling
endif # COOL

#----------------------------------------------------------------------#
.SUFFIXES: .cpp .o .sh

export VEL COOL FORCOMPILER CPPC NAMECPP NAMEFOR

sources = lineparser.cpp
objects = lineparser.o
execs = lineparser.x $(NAMECPP) $(NAMEFOR)

$(NAME).sh: $(execs)
	chmod +x $(NAME).sh

.cpp.o: $(sources)
	$(CPPC) -c $*.cpp

lineparser.x: $(objects)
	$(CPPC) $(objects) -o lineparser.x

SUBDIRS = $(NAMECPP) $(NAMEFOR)

subdirs: $(SUBDIRS)

$(SUBDIRS):
	$(MAKE) -C $@

clean:
	rm -rf *.o *.x *~

clean-all:
	rm -rf *.o *.x *~
	$(MAKE) -C $(NAMECPP) clean
	$(MAKE) -C $(NAMEFOR) clean 

run:
	./$(NAME).sh $(COOLDIR) $(TDIR)

test:
	./$(NAME).sh $(COOLDIR) $(TESTDIR)

#----------------------------------------------------------------------#
.PHONY: clean clean-all run test
.PHONY: sundirs $(SUBDIRS)
