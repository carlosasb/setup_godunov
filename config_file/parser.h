#include <vector>
#include <string>
#include <sstream>

using namespace std;

#ifndef PARSER
#define PARSER
int ntoken(const string &s, char delim);
string select_token(const string &s, char delim, int l);
vector<string> tokenize(const string &s, char delim);
#endif
