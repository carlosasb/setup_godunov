#include "parser.h"

int ntoken(const string &s, char delim)
{
    int number = 0;
    vector<string> elems;
    elems = tokenize(s,delim);
    number  = elems.size();
    return number;
}

string select_token(const string &s, char delim, int l)
{
    string token = "";
    vector<string> elems;
    elems = tokenize(s,delim);
    if (l <= elems.size())
    {
        token = elems[l];
    }
    return token;
}

vector<string> tokenize(const string &s, char delim)
{
    stringstream ss(s);
    string item;
    vector<string> elems;
    while (getline(ss,item,delim))
    {
        if (!item.empty())
        {
            elems.push_back(item);
        }
    }
    return elems;
}
