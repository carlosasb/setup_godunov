#include <iostream>
#include <fstream>
#include <iomanip>
#include "parser.h"

int main(int argc, char* argv[])
{
    char delim = ' ';
    int l = 0, n = (argc - 1)/2, nchar = 0, i;
    float val;
    string var, line1, line2, vartab = "";
    vector<string> varname;
    vector<float> varval;
#ifdef COOL
    ifstream init_file("template_cooling.in");
#else
    ifstream init_file("template.in");
#endif /* COOL */
    ofstream config_file("carlos.in");
    stringstream entry;

    for (int j = 2; j < argc; j+=2)
    {
        entry << argv[j-1] << '\n';
        getline(entry,line1);
        varname.push_back(line1);
        entry << argv[j] << '\n';
        getline(entry,line2);
        stringstream(line2) >> val;
        if (line1 == "sig")
        {
            val = val/1000.0;
        }
        if (line1 == "gamma")
        {
            val = val/10.0;
        }
        varval.push_back(val);
    }

    var = "";
    line1 = "";
    line2 = "";
    if (init_file.is_open())
    {
        while (getline(init_file,line1))
        {
            if (!line1.empty())
            {
                l = 0;
                var = select_token(line1,delim,0);
                for (int k = 0; k < n; k++)
                {
                    if (var == varname[k])
                    {
                        nchar = var.size();
                        for (i = 0; i < 8 - nchar; i++) {vartab = vartab + " ";}
                        config_file << var << vartab << " = " << fixed << setprecision(3) << varval[k] << '\n';
                        vartab = "";
                        k = n;
                        l = 1;
                    }
                }
                if (l != 1) {config_file << line1 << "\n";}
            } else
            {
                config_file << "" << '\n';
            }
        }
        line1 = "";
    }

    init_file.close();
    config_file.close();
    return 0;
}
